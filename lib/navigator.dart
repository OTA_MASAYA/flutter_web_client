@JS('_')
library flutter_web_client;

import 'package:js/js.dart';

@JS('navigator.MediaDevices.getUserMedia')
external String getUserMedia();
// {audio: true, video: false}