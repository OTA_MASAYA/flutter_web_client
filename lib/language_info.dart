// language_info.dart
//
//   Copyright (c) 2021 National Institute of Information and Communications Technology. All rights reserved.
//

import 'dart:ui';

import 'package:flutter/foundation.dart' show defaultTargetPlatform;
import 'package:flutter/foundation.dart' show TargetPlatform;

///
/// テキスト表示を行うときに必要な言語の情報を持つクラス
///
class LanguageInfo {
  // 言語コードからロケールへのマップ
  static const Map<String, Locale> _localeMap = {
    'ja': Locale('ja'),
    'zh': Locale.fromSubtags(
        languageCode: 'zh', scriptCode: 'Hans', countryCode: 'CN'),
    'zh_taiwan': Locale.fromSubtags(
        languageCode: 'zh', scriptCode: 'Hant', countryCode: 'TW'),
    'my': Locale('my'),
  };

  // 言語コードからフォントファミリーへのマップ (Android)
  static const Map<String, String> _fontFamilyAndroid = {
    'my': 'NotoSansMyanmar',
  };

  // 言語コードからフォントファミリーへのマップ (iOS)
  static const Map<String, String> _fontFamilyCupertino = {
    'ja': 'Hiragino sans',
    'zh': 'PingFang SC',
    'zh_taiwan': 'PingFang TC',
  };

  // 言語コード
  String _languageCode;

  // ロケール
  Locale? _locale;

  // フォントファミリー
  String? _fontFamily;

  List<String>? _fontFamilyFallback;

  ///
  /// 実行端末のプラットフォームからインスタンスを生成位する
  ///
  LanguageInfo(String languageCode)
      : this.platform(languageCode, defaultTargetPlatform);

  ///
  /// プラットフォームを指定してインスタンスを作成する
  ///
  LanguageInfo.platform(String this._languageCode, TargetPlatform platform) {
    // ロケールの設定
    _locale = _localeMap[_languageCode];

    // フォントファミリを決める
    if (platform == TargetPlatform.iOS) {
      _fontFamily = _fontFamilyCupertino[_languageCode];
    } else {
      _fontFamily = _fontFamilyAndroid[_languageCode];
      _fontFamilyFallback = [_fontFamilyAndroid['my'] ?? 'NotoSansMyanmar'];
    }
  }

  /// 言語コードを取得する
  String get languageCode => _languageCode;

  /// ロケールを取得する
  Locale? get locale => _locale;

  /// フォントファミリーを取得する
  String? get fontFamily => _fontFamily;

  // フォールバックフォントを取得する
  List<String>? get fontFamilyFallback => _fontFamilyFallback;
}
