import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

// ignore: must_be_immutable
class LanguageActivity extends StatefulWidget {
  bool? languageCodeAFlag;
  String? languageCodeA;
  String? languageCodeB;

  LanguageActivity(
      this.languageCodeA, this.languageCodeB, this.languageCodeAFlag);

  _LanguageActivity createState() => _LanguageActivity();
}

class _LanguageActivity extends State<LanguageActivity> {
  var _radVal;

  /// Shared Preferenceにデータを書き込む
  _setPrefItems(String key, String lang) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(key, lang);
  }

  /// ラジオボタンをタップした際の選択言語を変更する処理
  /// 言語Aと言語Bの言語コードを変更
  /// アプリ起動直後のメイン画面の結果フィールドに表示するテキスト変更
  void _onChanged(languageCode? value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _radVal = value;
      if (widget.languageCodeAFlag!) {
        if (widget.languageCodeB != _displayLanguageCode[value!.index])
          widget.languageCodeA = _displayLanguageCode[value.index];
        else {
          widget.languageCodeB = widget.languageCodeA;
          widget.languageCodeA = _displayLanguageCode[value.index];
          _setPrefItems(
              'languageCodeB',
              languageCode
                  .values[_displayLanguageCode.indexOf(widget.languageCodeB!)]
                  .toString()
                  .split('.')
                  .last);
        }
        _setPrefItems('languageCodeA', _radVal.toString().split('.').last);
      } else {
        if (widget.languageCodeA != _displayLanguageCode[value!.index])
          widget.languageCodeB = _displayLanguageCode[value.index];
        else {
          widget.languageCodeA = widget.languageCodeB;
          widget.languageCodeB = _displayLanguageCode[value.index];
          _setPrefItems(
              'languageCodeA',
              languageCode
                  .values[_displayLanguageCode.indexOf(widget.languageCodeA!)]
                  .toString()
                  .split('.')
                  .last);
        }
        _setPrefItems('languageCodeB', _radVal.toString().split('.').last);
      }
      _setPrefItems('displayLanguageCodeA', widget.languageCodeA!);
      _setPrefItems('displayLanguageCodeB', widget.languageCodeB!);
      _setPrefItems(
          'initResultFieldTextA',
          initResultFieldTextList[
              _displayLanguageCode.indexOf(widget.languageCodeA!)]);
      _setPrefItems(
          'initResultFieldTextB',
          initResultFieldTextList[
              _displayLanguageCode.indexOf(widget.languageCodeB!)]);
    });
  }

  /// 言語選択画面がAかBであるかのFlagの状態を変更
  void _languageCodeFlagChange() {
    setState(() {
      widget.languageCodeAFlag = !widget.languageCodeAFlag!;
    });
  }

  /// 選択されているラジオボタンの状態を変更
  void _radValChangeLanguageCode() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      if (widget.languageCodeAFlag!) {
        _radVal = languageCode
            .values[_displayLanguageCode.indexOf(widget.languageCodeA!)];
      } else {
        _radVal = languageCode
            .values[_displayLanguageCode.indexOf(widget.languageCodeB!)];
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _radValChangeLanguageCode();
  }

  /// 言語リストを作成
  List<Widget> languageListWidget() {
    var resultList = <Widget>[];
    for (int i = 0; i < languageListChar.length; i++) {
      resultList.add(
        RadioListTile<languageCode>(
            tileColor: Colors.white,
            title: Text(
              languageListChar[i] + '：' + _displayLanguageCode[i],
              style: TextStyle(locale: Locale("ja", "JP"), fontSize: 28.0),
            ),
            value: languageCode.values[i],
            groupValue: _radVal,
            onChanged: _onChanged),
      );
    }
    return resultList;
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> _languageListWidget = languageListWidget();
    return Scaffold(
      backgroundColor: Colors.grey[300],
      body: SafeArea(
        top: true,
        bottom: false,
        left: false,
        right: false,
        child: Row(children: [
          Expanded(
            flex: 15,
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 2.5),
                    child: Padding(
                      padding: EdgeInsets.only(right: 5.0),
                      child: InkWell(
                        onTap: () {
                          if (widget.languageCodeAFlag!)
                            Navigator.pop(context);
                          else {
                            _languageCodeFlagChange();
                            _radValChangeLanguageCode();
                          }
                        },
                        child: Container(
                          color: widget.languageCodeAFlag!
                              ? Colors.greenAccent[700]
                              : Colors.white,
                          child: Center(
                            child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    widget.languageCodeAFlag!
                                        ? widget.languageCodeA! + ' <'
                                        : widget.languageCodeA! + ' >',
                                    style: TextStyle(
                                        fontSize: 24,
                                        color: widget.languageCodeAFlag!
                                            ? Colors.white
                                            : Colors.black),
                                  ),
                                ]),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: EdgeInsets.only(top: 2.5),
                    child: Padding(
                      padding: EdgeInsets.only(right: 5.0),
                      child: InkWell(
                        onTap: () {
                          if (!widget.languageCodeAFlag!)
                            Navigator.pop(context);
                          else {
                            _languageCodeFlagChange();
                            _radValChangeLanguageCode();
                          }
                        },
                        child: Container(
                          color: !widget.languageCodeAFlag!
                              ? Colors.greenAccent[700]
                              : Colors.white,
                          child: Center(
                            child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Text(
                                    !widget.languageCodeAFlag!
                                        ? widget.languageCodeB! + ' <'
                                        : widget.languageCodeB! + ' >',
                                    style: TextStyle(
                                        fontSize: 24,
                                        color: !widget.languageCodeAFlag!
                                            ? Colors.white
                                            : Colors.black),
                                  ),
                                ]),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 85,
            child: ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return Container(
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(color: Colors.grey[300]!, width: 1.5),
                    ),
                  ),
                  //alignment: Alignment.topLeft,
                  child: SingleChildScrollView(
                    child: _languageListWidget[index],
                  ),
                );
              },
              itemCount: languageListChar.length,
            ),
          ),
        ]),
      ),
    );
  }
}

/// サーバに送信するための言語コード
enum languageCode {
  ja,
  en,
  zh,
  zh_taiwan,
  ko,
  th,
  fr,
  id,
  vi,
  es,
  my,
  fp,
  pt_brazil,
  km,
  ne,
  mn
}

/// アプリ内で表示するための言語コード（languageCodeと同順）
List<String> _displayLanguageCode = [
  'ja',
  'en',
  'zh-cn',
  'zh-tw',
  'ko',
  'th',
  'fr',
  'id',
  'vi',
  'es',
  'my',
  'fp',
  'pt-br',
  'km',
  'ne',
  'mn',
];

/// 日本語用の言語リスト（languageCodeと同順）
List<String> languageListChar = [
  '日本語',
  '英語',
  '中国語(簡体字)',
  '中国語(繁体字)',
  '韓国語',
  'タイ語',
  'フランス語',
  'インドネシア語',
  'ベトナム語',
  'スペイン語',
  'ミャンマー語',
  'フィリピン語',
  'ポルトガル語(ブラジル)',
  'クメール語',
  'ネパール語',
  'モンゴル語',
];

/// アプリ起動直後にメイン画面の結果フィールドに表示するテキストのリスト（languageCodeと同順）
List<String> initResultFieldTextList = [
  'マイクボタンをタップして話してください。',
  'Tap the microphone button and speak.',
  '请点击麦克风按钮并讲话。',
  '請點擊麥克風按鈕並說話。',
  '마이크 버튼을 누르고 말씀해 주십시오.',
  'กรุณาแตะปุ่มไมโครโฟนแล้วพูด',
  'Touchez le bouton micro et parlez.',
  'Ketuk tombol mikrofon dan bicaralah.',
  'Hãy nhấn vào nút \\\"micro\\\" và bắt đầu nói.',
  'Pulse el botón del micrófono y hable.',
  'Tap the microphone button and speak.',
  'Tap the microphone button and speak.',
  'Tap the microphone button and speak.',
  'Tap the microphone button and speak.',
  'Tap the microphone button and speak.',
  'Tap the microphone button and speak.',
];
